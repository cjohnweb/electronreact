**ElectronReact**

A simple React Application packaged into an Executable Electron Application

**Install project dependencies**

After cloning this project you'll want to

    npm install

*You will likely see this error at the end of npm install:

```
Error: /usr/local/Cellar/node/11.10.0/bin/node exited with code 1
Output:

> fsevents@1.2.7 install /Users/[...]/electronReact/node_modules/chokidar/node_modules/fsevents
> node install
```

You can ignore this warning, proceed as described and this shouldn't be a problem.

**Start the application in development mode**

After npm install finishes, you can then start your app with

    npm start
    
Note that in dev mode, the electron Browser is basically going to access your 
React app as it is served on localhost:3000, so if something else is running on 
port 3000 then you may not see your React app but the other thing running on that port.

Your React application files are located in the /src directory as they normally 
are in a React application. There are 2 additional files in this directory, 
start.js and start-react.js. The start.js file is basically the core setup for 
the Electron App, it may be modified further. ie default Window Size. It also 
changes the way the React application is pulled in based on development mode 
(packaging from localhost:3000) or production build (packaging from bundled source).

**Bundle your react application**

Bundling drops your entire React application into a single js file, it also 
writes a new HTML file that points to this new bundle, among a few other 
things. All of these new files are placed in the /build folder which will be 
automatically created if it doesn't already exist.)

    npm run build


**Preparing for packing into executable for distribution**
During development you will likely want to run the dev tools inspector - like 
you do in Chrome Browser. To do this uncomment line 23 in src/main.js. When you 
are ready to package up your app for distribution you'll want to comment this 
out again.

Once you have created your app and you are ready to compile the application 
into an executable you'll have to install electron-packager with the -g 
(global) command. This makes the electron-packager command available on your 
systems commandline. If you don't do this step, you'll see errors like 
`bash: electron-packager: command not found`

    npm install -g electron-packager

**Building the Executable**
The last step is to package your React App into an Electron Executable. Our 
src/start.js file for Electron will pull the index.html created from the build 
process and display it when your app runs, thereby also rendering your new 
React bundle in index.html. The start.js and package.json files must be configued 
correctly or you might just get a blank white page. If this is the case, I 
find it helpful to open up the dev tools (/src/start.js:23) and verify that 
my bundle is getting pulled in. It could be a web packer problem, or maybe you 
forgot to *npm run build* on the last step.

The command below is highly configurable, you'll need to modify it for your 
target operating system.

This is the command to build a application for MacOS and for x64 processor 
architecture. The icon parameter is optional, so I added a random icon to the 
project. This may take 30 seconds to a couple minutes depending on your system.

    electron-packager . --overwrite --platform=darwin --arch=x64 --icon=assets/icons/bolt_circle.icns --out=release-builds

If you get errors about permissions, try running the above command again with 
sudo. In my case, I probably installed electron-packager with root permissions 
and so I needed to use sudo to access it.

Google the electron-packager command for options to compile for other systems. 

And that's it, your new build should load into the release-builds folder, which 
should be created automatically if it does not already exist. Navigate to 
/release-builds and run your application!

**The default React Application**
The default React application in /src/App.js runs a setTimeout that calls itself repreatedly, 
updating the component state with the current time. This time is displayed in 
the render method. When you run it you should see a timestamp with milliseconds 
updating in real time.
