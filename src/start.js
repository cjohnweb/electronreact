const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow

const path = require('path')
const url = require('url')

let mainWindow

function createWindow() {
  mainWindow = new BrowserWindow({ width: 850, height: 600, })

  const startUrl = process.env.ELECTRON_START_URL || url.format({
    pathname: path.join(__dirname, '/../build/index.html'),
    protocol: 'file',
    hash: '',
    slashes: true
  });

  mainWindow.loadURL(startUrl);

  // Open devtools
  // mainWindow.webContents.openDevTools();

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  // On Mac applications quit kind of differently....they don't really quit until you force it to quick with command Q or something.
  if (process.platform !== 'darwin') {
    app.quit()
  } 
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})