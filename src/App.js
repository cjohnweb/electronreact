import React, { Component } from 'react';
import './css/font-awesome.min.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 'just one sec...'
    }

    this.theTimer = this.theTimer.bind(this);

  };

  componentDidMount() {
    // Launch the timer thing AFTER the component has mounted...
    this.theTimer();

  }

  theTimer() {

    // Have to bind the callback function here for some reason.
    // Reruns this function, which updates state with a time stamp, which triggers a rerender
    setTimeout(function () {
      this.theTimer();
    }.bind(this), 10);

    // Date Object
    let d = new Date();
    let hours = d.getHours().length < 2 ? '0' + d.getHours() : d.getHours()
    let mins = d.getMinutes().length < 2 ? '0' + d.getMinutes() : d.getMinutes()
    let secs = d.getSeconds().length < 2 ? '0' + d.getSeconds() : d.getSeconds()
    let milliSecs = d.getMilliseconds().length === 1 ?
      '00' + d.getMilliseconds() :
      d.getMilliseconds().length === 2 ?
        '0' + d.getMilliseconds() :
        d.getMilliseconds()

    let n = hours + ":" + mins + ":" + secs + " " + milliSecs;

    this.setState({ time: n });
  }


  render() {

    let time = this.state.time;

    return (
      <div style={{ width: '100%' }}>
        <h2 style={{ margin: '20px', padding: '20px' }}>Current Time: {time}</h2>
        <div style={{ margin: '14px', width: '400px', border: '0px solid #666666' }}>
          Put together in the stuppor of sleep deprivation by <a target={'_blank'} href={"http://johnminton.info"}>John Minton</a>, good luck, you're gonna need it!
            </div>
      </div>
    )

  }

}

export default App;
